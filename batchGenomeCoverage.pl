#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

my $pathSource = shift;
my $pathDest = shift;

my $nodes = 1;
my $partition = "bedtools genomecov";
my $time = "100:00:00";
my $ntasks = 1;
my $cpus = 128;
my $name = "braintest";

# print header
print "#!/bin/bash\n";
print "\n";
print "#SBATCH --nodes=$nodes\n";
print "#SBATCH --partition=$partition\n";
print "#SBATCH --time=$time\n";
print "#SBATCH --ntasks=$ntasks\n";
print "#SBATCH --cpus-per-task=$cpus\n";
print "#SBATCH --job-name=$name\n";
print "#SBATCH --error=error_\%j.out\n";
print "#SBATCH --output=output_\%j.out\n";
print "\n";
print "echo \$SLURM_SUBMIT_DIR\n";
print "echo \"Running on \`hostname\`\"\n";
print "\n";

my @fileList = glob($pathSource . "/*.bam");
foreach my $file (@fileList) {

    my $fileName = fileparse($file, ".bam");
    my $fileOut = $pathDest . "/" . $fileName . ".gbed";
    print "bedtools genomecov -ibam " . $file . " -bg -split | sort -k1,1 -k2,2n -k3,3n | bgzip > " . $fileOut . "\n";
    print "tabix --zero-based --sequence 1 --begin 2 --end 3 " . $fileOut . "\n";
}



