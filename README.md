# CellrangerPipeline
batch run for cellranger count 

```
perl batchCellrangerCounter.pl --help
version 1.0, October 2018
usage: batchCellrangerCounter.pl
-f|--fastq
	 path to FastQ files (required)
-o|--output-dir
	 path to output directory (required)
-g|--genome
	 path to genome index (required)
-p|--opts
	 additional Cellranger Count parameters
-h|--help
	 print help message
-v|--version
	 print current version
```

example:

```
perl batchCellrangerCounter.pl --fastq /path/to/fastq/ --output-dir /path/to/projec/cellranger_count_out/ --genome /path/to/genome/alien/cellranger/ --opts "--chemistry SC3Pv2 --localcores=32 --force-cells=7000"
```
